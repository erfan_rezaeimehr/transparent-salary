const ranks = [
	{
		id: 1,
		title: 'خاسته',
		subRanks: [
			{
				id: 1,
				title: 'نوخاسته',
				coefficient: 1,
				coefficientFa: '۱',
			},
			{
				id: 2,
				title: 'خاسته',
				coefficient: 1.5,
				coefficientFa: '۱.۵',
			},
		],
	},
	{
		id: 2,
		title: 'ساخته',
		subRanks: [
			{
				id: 1,
				title: 'نوساخته',
				coefficient: 2,
				coefficientFa: '۲',
			},
			{
				id: 2,
				title: 'ساخته',
				coefficient: 2.7,
				coefficientFa: '۲.۷',
			},
			{
				id: 3,
				title: 'کهن‌ساخته',
				coefficient: 3.3,
				coefficientFa: '۳.۳',
			},
		],
	},
	{
		id: 3,
		title: 'برومند',
		subRanks: [
			{
				id: 1,
				title: 'نوبرومند',
				coefficient: 4,
				coefficientFa: '۴',
			},
			{
				id: 2,
				title: 'برومند',
				coefficient: 4.7,
				coefficientFa: '۴.۷',
			},
			{
				id: 3,
				title: 'کهن‌برومند',
				coefficient: 5.3,
				coefficientFa: '۵.۳',
			},
		],
	},
	{
		id: 4,
		title: 'دلیر',
		subRanks: [
			{
				id: 1,
				title: 'نودلیر',
				coefficient: 6,
				coefficientFa: '۶',
			},
			{
				id: 2,
				title: 'دلیر',
				coefficient: 6.7,
				coefficientFa: '۶.۷',
			},
			{
				id: 3,
				title: 'کهن‌دلیر',
				coefficient: 7.3,
				coefficientFa: '۷.۳',
			},
		],
	},
	{
		id: 5,
		title: 'دلاور',
		subRanks: [
			{
				id: 1,
				title: 'نودلاور',
				coefficient: 8,
				coefficientFa: '۸',
			},
			{
				id: 2,
				title: 'دلاور',
				coefficient: 8.7,
				coefficientFa: '۸.۷',
			},
			{
				id: 3,
				title: 'کهن‌دلاور',
				coefficient: 9.3,
				coefficientFa: '۹.۳',
			},
		],
	},
	{
		id: 6,
		title: 'رادکسوت',
		subRanks: null,
		coefficient: 10,
		coefficientFa: '۱۰',
	},
	{
		id: 7,
		title: 'کهن‌کسوت',
		subRanks: null,
		coefficient: 10,
		coefficientFa: '۱۰',
	},
]

export function getRankByID(rankID) {
	return ranks.find(x => x.id === Number(rankID))
}

export default ranks
