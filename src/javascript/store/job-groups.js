const jobGroups = [
	{
		id: 1,
		titles: 'Leadership',
		averageSalary: '۴۴,۰۳۴,۵۰۰',
		coefficient: 3.5,
		coefficientFa: `۳.۵`,
	},
	{
		id: 2,
		titles: 'Business Architect, Finance Architect, Brand Manager, Tech Architect, Tech Product Manager',
		averageSalary: '۲۹,۷۸۶,۲۸۰',
		coefficient: 2.9,
		coefficientFa: '۲.۹',
	},
	{
		id: 3,
		titles: 'Executive Manager',
		averageSalary: '۲۳,۰۲۵,۴۸۰',
		coefficient: 2.6,
		coefficientFa: '۲.۶',
	},
	{
		id: 4,
		titles: 'SRE, Devops, Communication Architect',
		averageSalary: '۱۶,۹۵۵,۰۳۰',
		coefficient: 2.4,
		coefficientFa: '۲.۴',
	},
	{
		id: 5,
		titles: 'System Developer',
		averageSalary: '۱۳,۸۷۵,۱۵۰',
		coefficient: 2.3,
		coefficientFa: '۲.۳',
	},
	{
		id: 6,
		titles: 'Security Engineer, SysAdmin, NetworkAdmin, Backend, Frontend, Fullstack, Performance Marketer, Experience Designer, Business Developer, UI/UX Designer',
		averageSalary: '۱۵,۴۲۲,۶۴۰',
		coefficient: 2.2,
		coefficientFa: '۲.۲',
	},
	{
		id: 7,
		titles: 'Communication Specilist, Content Expert, Account Manager, Human Capital Developer, Project Manager, Graphic designer, Market Researcher, Active Sales',
		averageSalary: '۱۲,۵۸۳,۶۰۰',
		coefficient: 2,
		coefficientFa: '۲',
	},
	{
		id: 8,
		titles: 'Accountant, Coordinator, Bi Specialist, Traffic Manager, Passive Sales, Helpdesk Specialist, Data Center Engineer, Cloud Engineer',
		averageSalary: '۹,۰۸۶,۵۰۰',
		coefficient: 1.9,
		coefficientFa: '۱.۹',
	},
	{
		id: 9,
		titles: 'Office Executive',
		averageSalary: '۸,۴۱۵,۳۹۰',
		coefficient: 1.6,
		coefficientFa: '۱.۶',
	},
]

export function getJobGroupByID(groupID) {
	return jobGroups.find(x => x.id === Number(groupID))
}

export default jobGroups
