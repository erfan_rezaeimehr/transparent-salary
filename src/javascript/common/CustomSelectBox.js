class CustomSelectBox {
	constructor(currentElementID, listElementID, callback) {
		this.currentElement = document.getElementById(currentElementID)
		this.listElement = document.getElementById(listElementID)

		this.currentElement.addEventListener('click', this.openList)
		this.listElement.addEventListener('click', this.handleSelectOption.bind(this))

		document.addEventListener('click', ({ target }) => {
			if (this.listElement.contains(target)) return
			if (!this.isListOpen()) return
			if (target === this.currentElement) return
			if (target === this.currentElement.children[1]) return

			this.closeLists()
		})

		this.selectedOption = null
		this.selectedOptionValue = null
		this.callback = callback
	}

	isListOpen() {
		return this.listElement.classList.contains('select-box__list--open')
	}

	isAnyOptionSelected(el) {
		return el.querySelectorAll('.select-box__list-item--active').length > 0
	}

	validateSelectOptionClick(el) {
		return el.tagName.toLowerCase() === 'li' || el.classList.contains('select-box__list-item')
	}

	removePreviousSelectedOptionClass(el) {
		if (this.isAnyOptionSelected(el.parentElement)) {
			const previousSelectedOption = el.parentElement.querySelector('.select-box__list-item--active')
			previousSelectedOption.classList.remove('select-box__list-item--active')
		}
	}

	activeSelectedOptionClass(el) {
		el.classList.add('select-box__list-item--active')
	}

	activeArrowIconClass(el) {
		const arrowIconEl = el.parentElement.previousElementSibling.children[0]
		arrowIconEl.classList.add('select-box__arrow--active')
	}

	updateSelectedOptionTitle(el) {
		const selectedOptionTitleEl = el.parentElement.previousElementSibling.children[1]
		selectedOptionTitleEl.innerText = el.innerText
	}

	openList({ currentTarget }) {
		const currentElement = currentTarget
		const listElement = currentTarget.nextElementSibling
		const arrowIconEl = currentTarget.children[0]

		currentElement.classList.add('select-box__current--open')
		listElement.classList.add('select-box__list--open')
		arrowIconEl.classList.add('select-box__arrow--open')
	}

	handleSelectOption({ target }) {
		if (!this.validateSelectOptionClick(target)) return

		this.selectedOption = target

		this.removePreviousSelectedOptionClass(this.selectedOption)

		this.activeSelectedOptionClass(this.selectedOption)

		this.activeArrowIconClass(this.selectedOption)

		this.updateSelectedOptionTitle(this.selectedOption)

		this.closeList(this.selectedOption.parentElement.previousElementSibling)

		this.selectedOptionValue = this.selectedOption.attributes['data-value'].value

		this.callback(this.selectedOptionValue, this.selectedOption)
	}

	closeList(el) {
		const currentElement = el.nextElementSibling
		const arrowIconEl = el.children[0]

		el.classList.remove('select-box__current--open')
		currentElement.classList.remove('select-box__list--open')
		arrowIconEl.classList.remove('select-box__arrow--open')
	}

	closeLists() {
		document.querySelectorAll('.select-box__current').forEach(currentEl => {
			this.closeList(currentEl)
		})
	}
}

export default CustomSelectBox
