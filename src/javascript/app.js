import 'bootstrap'
import '../sass/styles.scss'

import './sections/calculator'
import './sections/transparency'
import './sections/factors'
