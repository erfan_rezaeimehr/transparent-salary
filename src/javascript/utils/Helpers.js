export function enToFaDigits(number) {
	let looper = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹']
	let englishDigits = Array.from(String(number), Number)
	let farsiDigits = []
	englishDigits.forEach(digit => {
		farsiDigits.push(looper[digit])
	})

	return farsiDigits.join('')
}

export function numberWithCommas(n) {
	let output = ''
	for (let i = 0; i < n.length; i++) {
		let c = n.substr(n.length - i - 1, 1)
		if ((i % 3 == 0) & (i > 0)) {
			output = c + ',' + output
		} else {
			output = c + output
		}
	}
	return output
}
