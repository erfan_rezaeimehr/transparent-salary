import CustomSelectBox from '../common/CustomSelectBox'
import { getJobGroupByID } from '../store/job-groups'
import { getRankByID } from '../store/ranks'
import { getJobTitleByID } from '../store/job-titles'
import { enToFaDigits, numberWithCommas } from '../utils/helpers'

new CustomSelectBox('calc-select-job-title', 'calc-job-titles-list', handleSelectJobTitle)

document.getElementById('btn-calculate-salary').addEventListener('click', calculateSalary)

document.querySelectorAll('.salary-details__calc-rank').forEach(element => {
	element.addEventListener('click', handleSelectRank)
})

let selectedRankID = null
let selectedJobTitleID = null

function handleSelectRank(event) {
	removePreviousActiveRankClass()

	const clickedRankEl = event.currentTarget
	clickedRankEl.classList.add('salary-details__calc-rank--active')
	selectedRankID = clickedRankEl.attributes['data-value'].value
}

function removePreviousActiveRankClass() {
	document.querySelectorAll('.salary-details__calc-rank').forEach(element => {
		element.classList.remove('salary-details__calc-rank--active')
	})
}

function handleSelectJobTitle(jobTitleID) {
	selectedJobTitleID = jobTitleID
}

function getMinimumRankCoefficient(selectedRank) {
	if (selectedRank.subRanks) {
		return selectedRank.subRanks[0].coefficient
	}

	return selectedRank.coefficient
}

function getMaximumRankCoefficient(selectedRank) {
	if (selectedRank.subRanks) {
		return selectedRank.subRanks[selectedRank.subRanks.length - 1].coefficient
	}

	return selectedRank.coefficient
}

function updateSalaries(minSalary, maxSalary) {
	const minSalaryEl = document.getElementById('min-salary')
	const maxSalaryEl = document.getElementById('max-salary')

	const minSalaryStr = numberWithCommas(enToFaDigits(Math.floor(minSalary)))
	const maxSalaryStr = numberWithCommas(enToFaDigits(Math.floor(maxSalary)))

	minSalaryEl.textContent = minSalaryStr
	maxSalaryEl.textContent = maxSalaryStr
}

function calculateSalary() {
	if (!selectedRankID || !selectedJobTitleID) return

	const jobTitleObject = getJobTitleByID(selectedJobTitleID)

	const jobGroupID = jobTitleObject.jobGroupId

	const jobDifficultyCoefficient = jobTitleObject.jobDifficultyCoefficient

	const jobGroupCoefficient = getJobGroupByID(jobGroupID).coefficient

	const selectedRank = getRankByID(selectedRankID)

	let minimumRankCoefficient = getMinimumRankCoefficient(selectedRank)
	let maximumRankCoefficient = getMaximumRankCoefficient(selectedRank)

	let minSalary = (jobDifficultyCoefficient * jobGroupCoefficient * 1.2 * minimumRankCoefficient + 3) * 1000000
	let maxSalary = (jobDifficultyCoefficient * jobGroupCoefficient * 1.2 * maximumRankCoefficient + 3) * 1000000

	updateSalaries(minSalary, maxSalary)
}
