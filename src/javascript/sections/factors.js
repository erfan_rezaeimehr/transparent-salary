import CustomSelectBox from '../common/CustomSelectBox'
import { getRankByID } from '../store/ranks'
import { getJobGroupByID } from '../store/job-groups'

document.querySelectorAll('.salary-factors__tab').forEach(tab => {
	tab.addEventListener('click', handleTabClick)
})
function handleTabClick(event) {
	removePreviousActiveTabClasses()

	const clickedTabEl = event.currentTarget
	clickedTabEl.classList.add('salary-factors__tab--active')

	const clickedTabID = clickedTabEl.attributes['data-value'].value

	const boxContentEl = document.getElementById(`salary-factors_box-content_${clickedTabID}`)
	boxContentEl.classList.add('salary-factors__box-content--active')
}
function removePreviousActiveTabClasses() {
	document.querySelectorAll('.salary-factors__tab').forEach(tab => {
		tab.classList.remove('salary-factors__tab--active')
	})

	document.querySelectorAll('.salary-factors__box-content').forEach(tab => {
		tab.classList.remove('salary-factors__box-content--active')
	})
}

document.querySelectorAll('.salary-factors__rank-item').forEach(tab => {
	tab.addEventListener('click', handleRankClick)
})
function handleRankClick(event) {
	removePreviousActiveRankClass()

	const clickedRankEl = event.currentTarget
	clickedRankEl.classList.add('salary-factors__rank-item--active')
}
function removePreviousActiveRankClass() {
	document.querySelectorAll('.salary-factors__rank-item').forEach(tab => {
		tab.classList.remove('salary-factors__rank-item--active')
	})
}

new CustomSelectBox('select-rank', 'rank-list', handleSelectRank)
function handleSelectRank(rankID) {
	const rank = getRankByID(rankID)
	const subRanks = rank.subRanks

	let output = ''

	if (subRanks) {
		subRanks.forEach(subrank => {
			output += `<div class="salary-factors__rank-box-item">
                            <div>${subrank.title}</div>
                            <span>${subrank.coefficientFa}</span>
                        </div>`
		})
	} else {
		output = `<div class="salary-factors__rank-box-item">
                    <div>${rank.title}</div>
                    <span>${rank.coefficientFa}</span>
                  </div>`
	}

	const factorRankBoxEl = document.getElementById('factors-rank-box')
	factorRankBoxEl.classList.add('salary-factors__rank-box--active')
	factorRankBoxEl.innerHTML = output
}

new CustomSelectBox('select-position', 'positions-list', handleSelectPosition)
function handleSelectPosition(jobGroupId) {
	const jobGroupCoefficientEl = document.getElementById('job-group-coefficient')
	jobGroupCoefficientEl.textContent = getJobGroupByID(jobGroupId).coefficientFa
}

document.querySelectorAll('.salary-factors__difficult-job-position').forEach(element => {
	element.addEventListener('click', handleSelectDifficultJob)
})
function handleSelectDifficultJob(event) {
	removePreviousActiveDifficultJobClasses()
	const el = event.currentTarget
	el.classList.add('salary-factors__difficult-job-position--active')
	el.children[0].classList.add('salary-factors__difficult-job-position-title--active')
	el.children[1].classList.add('salary-factors__difficult-job-position-text--active')
}
function removePreviousActiveDifficultJobClasses() {
	document.querySelectorAll('.salary-factors__difficult-job-position').forEach(el => {
		el.classList.remove('salary-factors__difficult-job-position--active')
		el.children[0].classList.remove('salary-factors__difficult-job-position-title--active')
		el.children[1].classList.remove('salary-factors__difficult-job-position-text--active')
	})
}

document.querySelectorAll('.salary-factors__loyalty-line-item').forEach(element => {
	element.addEventListener('click', handleClickLoyalty)
})
function handleClickLoyalty(event) {
	removePreviousActiveLoyaltyClass()

	const clickedLoyaltyEl = event.currentTarget
	clickedLoyaltyEl.classList.add('salary-factors__loyalty-line-item--active')
}
function removePreviousActiveLoyaltyClass() {
	document.querySelectorAll('.salary-factors__loyalty-line-item').forEach(element => {
		element.classList.remove('salary-factors__loyalty-line-item--active')
	})
}

new CustomSelectBox('select-loyalty-years', 'loyalty-years-list', handleSelectLoyalty)
function handleSelectLoyalty(loyaltyCoefficient) {
	const loyaltyCoefficientEl = document.getElementById('loyalty-coefficient')
	loyaltyCoefficientEl.textContent = loyaltyCoefficient
}
