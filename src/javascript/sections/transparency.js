import CustomSelectBox from '../common/CustomSelectBox'
import { getJobGroupByID } from '../store/job-groups'

new CustomSelectBox('transparency-select-position', 'transparency-positions-list', handleSelectJobGroup)

function handleSelectJobGroup(jobGroupId) {
	const transparencyAverageSalaryEl = document.getElementById('transparency-average-salary')
	transparencyAverageSalaryEl.textContent = getJobGroupByID(jobGroupId).averageSalary
}
