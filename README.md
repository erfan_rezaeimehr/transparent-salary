# Transparent Salary

## Project setup
```
npm install
```

### Run this to process source files, build, and watch files for subsequent changes.
```
npm run dev
```

### Run this to process source files, build.
```
npm run build
```